/**
 * min and max value
 * @type {number}
 */
var min = 0,
    max = 10;

/**
 * user input
 * @type {Element}
 */
var myNumber = document.querySelector('#myNumber');

/**
 * setting min and max values
 */
myNumber.setAttribute('min', min);
myNumber.setAttribute('max', max);

/**
 * get a random number between min and max
 * @param min
 * @param max
 * @returns {*}
 */
function getRandomNumber(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * register to submit button click event
 * @param randomNumber
 */
function registerEvent(randomNumber) {
    var submit = document.querySelector('#submit');
    submit.addEventListener("click", function (e) {
        e.preventDefault();
        verifyNumber(randomNumber);
    });
}

/**
 * compare user and random number
 * @param randomNumber
 */
function verifyNumber(randomNumber) {
    var userNumber = getUserNumber();
    var result = document.querySelector("#result");
    purgeResults();
    if (userNumber < randomNumber) {
        console.log('your value :', userNumber, 'is lower than :', randomNumber);
        showResult('lower');
        return;
    }
    if (userNumber == randomNumber) {
        console.log('your value :', userNumber, 'is equal to :', randomNumber);
        showResult('equal');
        return;
    }
    if (userNumber > randomNumber) {
        console.log('your value :', userNumber, 'is greater than :', randomNumber);
        showResult('greater');
        return;
    }
}

/**
 * purge results
 */
function purgeResults() {
    document.querySelector("#lower").style.display = 'none';
    document.querySelector("#greater").style.display = 'none';
    document.querySelector("#equal").style.display = 'none';
}

/**
 * show result
 * @param type
 */
function showResult(type) {
    document.querySelector('#' + type).style.display = 'block';
}

/**
 * get user nuber
 * @returns {string|Number}
 */
function getUserNumber() {
    return document.getElementById("myNumber").value;
}

(function () {
    /**
     * random number between min and max
     */
    var randomNumber = getRandomNumber(min, max);

    /**
     * register to submit button click event
     */
    registerEvent(randomNumber);
})();

