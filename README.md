# Exercice javascript : jeu du plus ou moins
Le but du jeu : faire deviner un nombre choisi au hasard par l'ordinateur entre 1 et 100.

Vous allez devoir réaliser ce jeu en plusieurs étapes :

Le programme tire un nombre entre 1 et 100.
L'utilisateur saisie un nombre.
Le programme indique si ce nombre est inférieur, supérieur ou égal au tirage.
Votre code sera réparti entre 3 fichiers : 

un fichier index.html
un fichier script.js (dans un dossier js)
un fichier style.css (dans un dossier css)